import { css } from "@emotion/css";
import { useState } from "react";
import { useQuery } from "react-query";
import "./App.css";
import PersonInfo from "./components/PersonInfo";
import PlanetInfo from "./components/PlanetInfo";

enum SelectionType {
  none = "none",
  planets = "planets",
  people = "people",
}

const fetchData = async (type: any) => {
  if (type.queryKey[0] === SelectionType.none) {
    return {
      data: [],
      isLoading: true,
      error: new Error("Missing selection"),
    };
  }
  const response = await fetch(`http://localhost:4000/${type.queryKey[0]}/`);
  return response.json();
};

function App() {
  const [selectedType, setSelectedType] = useState<SelectionType>(
    SelectionType.none
  );
  const [selectedId, setSelectedId] = useState();
  const { data, isLoading, error } = useQuery<any, any>(
    selectedType,
    fetchData as any
  );

  let selectedItem;
  if (selectedId) {
    selectedItem = data?.find((item: any) => item.url === selectedId);
  }

  return (
    <div className="card">
      <h1
        className={css`
          color: #597eaa;
        `}
      >
        Star Wars API
      </h1>
      <div>
        <button
          onClick={() => {
            setSelectedId(undefined);
            setSelectedType(SelectionType.planets);
          }}
        >
          Planets
        </button>
        <button
          onClick={() => {
            setSelectedId(undefined);
            setSelectedType(SelectionType.people);
          }}
        >
          People
        </button>
      </div>
      {error && <div>Error: {error.message}</div>}
      {isLoading && <div>Loading...</div>}
      {!isLoading && selectedType !== SelectionType.none && (
        <div
          className={css`
            display: flex;
            gap: 20px;
            @media only screen and (max-width: 700px) {
              flex-direction: column;
            }
          `}
        >
          <div
            className={css`
              width: 280px;
            `}
          >
            <h2>
              List /{" "}
              <span
                className={css`
                  color: #6fa8dc;
                `}
              >
                {selectedType[0].toUpperCase() + selectedType.slice(1)}
              </span>
            </h2>
            <div
              className={css`
                font-size: 20px;
                display: flex;
                justify-content: space-between;
                margin-bottom: 0.4em;
              `}
            >
              <div>Name</div>
              <div># of Films</div>
            </div>
            <div
              className={css`
                max-height: 50vh;
                overflow: auto;
                display: flex;
                flex-direction: column;
                background-color: white;
              `}
            >
              {data.map &&
                data?.map((item: any) => (
                  <div
                    className={css`
                      color: #474747;
                      padding: 2px 10px;
                      cursor: pointer;
                      &:hover {
                        background-color: lightblue;
                      }
                    `}
                    style={{
                      backgroundColor:
                        item.url === selectedId ? "lightgray" : undefined,
                    }}
                    key={item.url}
                    onClick={() => setSelectedId(item.url)}
                  >
                    <div
                      className={css`
                        display: flex;
                        justify-content: space-between;
                      `}
                    >
                      <div>{item.name}</div>
                      <div>
                        {item.films ? `(${item.films?.length})` : undefined}
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
          <div
            className={css`
              width: 100%;
              flex-grow: 1;
              overflow: auto;
            `}
          >
            <h2>Info</h2>
            {selectedId && selectedType === SelectionType.people && (
              <PersonInfo person={selectedItem} />
            )}
            {selectedId && selectedType === SelectionType.planets && (
              <PlanetInfo planet={selectedItem} />
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
