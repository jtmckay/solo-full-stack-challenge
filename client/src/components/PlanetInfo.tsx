import Info from "./Info";
import { Person } from "./PersonInfo";

export type Planet = {
  name: string;
  rotation_period: string;
  orbital_period: string;
  diameter: string;
  climate: string;
  gravity: string;
  terrain: string;
  surface_water: string;
  population: string;
  residents: Person[];
};

export default function Planet({ planet }: { planet: Planet }) {
  return (
    <div>
      <Info title="Name" value={planet.name} />
      <Info title="Rotation Period" value={planet.rotation_period} />
      <Info title="Orbital Period" value={planet.orbital_period} />
      <Info title="Diameter" value={planet.diameter} />
      <Info title="Climate" value={planet.climate} />
      <Info title="Gravity" value={planet.gravity} />
      <Info title="Terrain" value={planet.terrain} />
      <Info title="Surface Water" value={planet.surface_water} />
      <Info title="Population" value={planet.population} />
    </div>
  );
}
