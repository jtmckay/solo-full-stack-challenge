import Info from "./Info";

export type Person = {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: any[];
  vehicles: string[];
  starships: string[];
  created: string;
  edited: string;
  url: string;
};

export default function PersonInfo({ person }: { person: Person }) {
  return (
    <div>
      <Info title="Name" value={person.name} />
      <Info title="Height" value={person.height} />
      <Info title="Weight" value={person.mass} />
      <Info title="Birth Year" value={person.birth_year} />
      <Info title="Eye Color" value={person.eye_color} />
    </div>
  );
}
