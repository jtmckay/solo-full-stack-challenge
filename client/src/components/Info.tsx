import { css } from "@emotion/css";

export default function Info({
  title,
  value,
}: {
  title: string;
  value: string;
}) {
  return (
    <div
      className={css`
        display: flex;
        border: black 4px solid;
        border-radius: 4px;
        margin: 8px;
        padding: 8px;
      `}
    >
      <div
        className={css`
          width: 140px;
        `}
      >
        {title}
      </div>
      <div>{value}</div>
    </div>
  );
}
