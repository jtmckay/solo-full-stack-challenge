const planets = require("./datasource").planets;

module.exports = (app) => {
  app.get("/planets", (req, res) => {
    res.send(planets);
  });
};
