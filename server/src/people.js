const people = require("./datasource").people;

module.exports = (app) => {
  app.get("/people", (req, res) => {
    let sortedPeople = people;
    const sortBy = req.query.sortBy;

    if (sortBy === "name") {
      sortedPeople.sort((a, b) => (a.name > b.name ? 1 : -1));
    } else if (sortBy === "height") {
      sortedPeople.sort((a, b) => (a.height > b.height ? 1 : -1));
    } else if (sortBy === "mass") {
      sortedPeople.sort((a, b) => (a.mass > b.mass ? 1 : -1));
    }

    res.send(sortedPeople);
  });
};
