const axios = require("axios");

const people = new Map();
const planets = new Map();

module.exports.people = [];
module.exports.planets = [];

module.exports.load = async function load() {
  (await allSwapi("https://swapi.dev/api/people/")).forEach((person) => {
    people.set(person.url, person);
  });
  module.exports.people.push(...Array.from(people.values()));

  (await allSwapi("https://swapi.dev/api/planets/")).forEach((planet) => {
    planets.set(planet.url, {
      ...planet,
      residents: planet.residents.map((resident) => people.get(resident).name),
    });
  });
  module.exports.planets.push(...Array.from(planets.values()));
};

async function allSwapi(url) {
  const results = [];
  let nextPage = url;
  while (nextPage) {
    const response = await axios.get(nextPage);
    results.push(...response.data.results);
    nextPage = response.data.next;
  }
  return results;
}
